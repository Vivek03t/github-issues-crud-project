const owner = 'tivivek';
const repo = 'github-issues-project';
const apiUrl = `https://api.github.com/repos/${owner}/${repo}/issues`;
const token = '';

// Function to make a GET request to the GitHub API
async function fetchFromGitHub(url, method = 'GET', body = null) {
  try {
    const response = await fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `token ${token}`,
      },
      body: body ? JSON.stringify(body) : null,
    });

    if (!response.ok) {
      throw new Error('Request failed');
    }

    return await response.json();
  } catch (error) {
    console.error(error);
    alert('An error occurred while making the request.');
    throw error;
  }
}

// Function to populate the issue list
function populateIssueList(issues) {
  const issueList = document.querySelector('.issue-list');
  issueList.innerHTML = '';

  issues.forEach((issue) => {
    const issueItem = document.createElement('li');
    issueItem.classList.add('issue');
    issueItem.id = `issue-${issue.number}`;
    issueItem.innerHTML = `
      <div class="issue-info">
        <h3>${issue.title}</h3>
        <p>${issue.body}</p>
      </div>
      <div class="issue-actions">
        <button class="close-button" data-issue-number="${issue.number}">Close</button>
        <button class="update-button" data-issue-number="${issue.number}">Update</button>
      </div>
    `;

    issueList.appendChild(issueItem);
  });

  // Add event listeners to update and close buttons
  const updateButtons = document.querySelectorAll('.update-button');
  const closeButtons = document.querySelectorAll('.close-button');

  updateButtons.forEach((button) => {
    button.addEventListener('click', () => {
      const issueNumber = button.getAttribute('data-issue-number');
      openUpdateModal(issueNumber);
    });
  });

  closeButtons.forEach((button) => {
    button.addEventListener('click', async () => {
      const issueNumber = button.getAttribute('data-issue-number');
      try {
        await toggleIssueState(issueNumber, 'closed');

        fetchIssues();
        alert('Issue closed successfully!');
      } catch (error) {
        console.error(error);
      }
    });
  });
}

// Function to open the update issue modal
function openUpdateModal(issueNumber) {
  const modal = document.getElementById('updateIssueModal');
  modal.style.display = 'block';

  const issueTitle = document.querySelector(
    `#issue-${issueNumber} .issue-info h3`
  ).textContent;

  const issueDescription = document.querySelector(
    `#issue-${issueNumber} .issue-info p`
  ).textContent;

  document.getElementById('update-title').value = issueTitle;
  document.getElementById('update-description').value = issueDescription;

  document
    .getElementById('updateIssueModal')
    .setAttribute('data-issue-number', issueNumber);
}

// Function to close the update issue modal
function closeUpdateModal() {
  const modal = document.getElementById('updateIssueModal');
  modal.style.display = 'none';

  fetchIssues();
}

// Event listener to close the modal when the close button is clicked
document.getElementById('closeModal').addEventListener('click', () => {
  closeUpdateModal();
});

// Event listener to submit the update form in the modal
document.getElementById('update-form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const updatedTitle = document.getElementById('update-title').value;
  const updatedDescription =
    document.getElementById('update-description').value;

  const issueNumber = document
    .getElementById('updateIssueModal')
    .getAttribute('data-issue-number');

  try {
    await updateIssue(issueNumber, updatedTitle, updatedDescription);
    closeUpdateModal();
    fetchIssues();
    alert('Issue updated successfully!');
  } catch (error) {
    console.error(error);
  }
});

// Function to create a new issue
async function createIssue(title, description) {
  try {
    await fetchFromGitHub(apiUrl, 'POST', {
      title,
      body: description,
    });

    fetchIssues();
    alert('Issue created successfully!');
  } catch (error) {
    console.error(error);
  }
}

// Event listener to create a new issue
document.getElementById('issue-form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const title = document.getElementById('issue-title').value;
  const description = document.getElementById('issue-description').value;

  try {
    await createIssue(title, description);
    // reset
    document.getElementById('issue-title').value = '';
    document.getElementById('issue-description').value = '';
  } catch (error) {
    console.error(error);
  }
});

// Function to toggle the state of an issue (open/close)
async function toggleIssueState(issueNumber, state) {
  try {
    await fetchFromGitHub(`${apiUrl}/${issueNumber}`, 'PATCH', {
      state: state,
    });

    fetchIssues();
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// Function to update an issue
async function updateIssue(issueNumber, updatedTitle, updatedDescription) {
  try {
    await fetchFromGitHub(`${apiUrl}/${issueNumber}`, 'PATCH', {
      title: updatedTitle,
      body: updatedDescription,
    });

    fetchIssues();
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// Function to fetch issues
async function fetchIssues() {
  try {
    const issues = await fetchFromGitHub(apiUrl);
    populateIssueList(issues);
  } catch (error) {
    console.error(error);
  }
}

// Initial fetch of issues
fetchIssues();
