# GitHub Issues Project

This project is a simple web application that allows you to create and manage GitHub issues for a specific repository. It provides basic functionality to create new issues, view existing issues, update issue details, and close issues.

## Table of Contents

- [GitHub Issues Project](#github-issues-project)
  - [Table of Contents](#table-of-contents)
  - [Demo](#demo)
  - [Features](#features)
  - [Installation](#installation)
  - [Usage](#usage)
    - [Create a new issue:](#create-a-new-issue)
    - [View existing issues:](#view-existing-issues)
    - [Update an issue:](#update-an-issue)
    - [Close an issue:](#close-an-issue)

## Demo

You can see a live demo of this project [here](https://github-issues-crud-project.vercel.app/).

## Features

- Create new GitHub issues with titles and descriptions.
- View a list of open issues from a specific GitHub repository.
- Update issue titles and descriptions.
- Close issues directly from the web application.

## Installation

1. Clone the repository to your local machine:

   ```sh
   git clone https://gitlab.com/Vivek03t/github-issues-crud-project

2. Navigate to the project directory:
   cd github-issues-project

3. Open the index.html file in your web browser.


## Usage
    Launch the web application by opening the index.html file.

### Create a new issue:

   - Enter a title and description for the     issue.
    - Click the "Create Issue" button.

### View existing issues:

   - Scroll down to the "Open Issues" section to see a list of open issues.
  
### Update an issue:

   - Click the "Update" button next to an issue in the list.
  
   - Modify the title and description as needed in the modal.

   - Click the "Save" button to update the issue.

### Close an issue:

   - Click the "Close" button next to an issue in the list to mark it as closed.
