const owner = 'tivivek';
const repo = 'github-issues-project';
const apiUrl = `https://api.github.com/repos/${owner}/${repo}/issues`;
const token = '';

const issueTitle = document.getElementById('issue-title');
const issueBody = document.getElementById('issue-body');
const createIssueButton = document.getElementById('create-issue');
const issueItems = document.getElementById('issue-items');

// Function to fetch issues
async function fetchIssues() {
  try {
    const response = await fetch(apiUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `token ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error('Unable to fetch issues');
    }

    const issues = await response.json();

    const issueList = document.querySelector('.issue-list');
    issueList.innerHTML = '';
    issues.forEach((issue) => {
      const issueItem = document.createElement('li');
      issueItem.id = `issue-${issue.number}`;
      issueItem.innerHTML = `
      <div class="issue-info">
        <h3>${issue.title}</h3>
        <p>${issue.body}</p>
      </div>
      <div class="issue-actions">
        <button class="close-button" data-issue-number="${issue.number}">Close</button>
        <button class="update-button" data-issue-number="${issue.number}">Update</button>
      </div>      
      `;
      issueList.appendChild(issueItem);

      const updateButton = issueItem.querySelector('.update-button');
      updateButton.addEventListener('click', () => {
        openUpdateModal(issue.number);
      });
    });
  } catch (error) {
    console.error(error);
    alert('An error occurred while fetching the issues.');
  }
}

// Function to open the update issue modal
function openUpdateModal(issueNumber) {
  const modal = document.getElementById('updateIssueModal');
  modal.style.display = 'block';

  const issueTitle = document.querySelector(
    `#issue-${issueNumber} .issue-info h3`
  ).textContent;
  const issueDescription = document.querySelector(
    `#issue-${issueNumber} .issue-info p`
  ).textContent;

  document.getElementById('update-title').value = issueTitle;
  document.getElementById('update-description').value = issueDescription;

  document
    .getElementById('updateIssueModal')
    .setAttribute('data-issue-number', issueNumber);
}

// Function to close the update issue modal
function closeUpdateModal() {
  const modal = document.getElementById('updateIssueModal');
  modal.style.display = 'none';
}

document.getElementById('closeUpdateModal').addEventListener('click', () => {
  closeUpdateModal();
});

// Event listener to submit the update form in the modal
document.getElementById('update-form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const updatedTitle = document.getElementById('update-title').value;
  const updatedDescription =
    document.getElementById('update-description').value;

  const issueNumber = document
    .getElementById('updateIssueModal')
    .getAttribute('data-issue-number');

  try {
    const response = await fetch(`${apiUrl}/${issueNumber}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `token ${token}`,
      },
      body: JSON.stringify({
        title: updatedTitle,
        body: updatedDescription,
      }),
    });

    if (response.ok) {
      fetchIssues();
      closeUpdateModal();
    } else {
      throw new Error('Unable to update issue');
    }
  } catch (error) {
    console.error(error);
  }
});

// Initial fetch of issues
fetchIssues();

// function to create issue
async function createIssue(title, description) {
  try {
    const response = await fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `token ${token}`,
      },
      body: JSON.stringify({
        title: title,
        body: description,
      }),
    });

    if (!response.ok) {
      throw new Error('Unable to create issue');
    }

    fetchIssues();

    // console.log('Issue created successfully');
  } catch (error) {
    console.error(error);
    alert('An error occurred while creating the issue.');
  }
}

// Event listener to update and close issues
document.addEventListener('click', async (event) => {
  if (
    event.target.classList.contains('close-button') ||
    event.target.classList.contains('update-button')
  ) {
    const issueNumber = event.target.getAttribute('data-issue-number');
    const action = event.target.classList.contains('close-button')
      ? 'close'
      : 'update';

    if (action === 'update') {
      openUpdateModal();

      const issueTitle = document.querySelector(
        `#issue-${issueNumber} .issue-info h3`
      ).textContent;
      const issueDescription = document.querySelector(
        `#issue-${issueNumber} .issue-info p`
      ).textContent;

      document.getElementById('update-title').value = issueTitle;
      document.getElementById('update-description').value = issueDescription;

      document
        .getElementById('updateIssueModal')
        .setAttribute('data-issue-number', issueNumber);
    }

    try {
      const response = await fetch(`${apiUrl}/${issueNumber}`, {
        method: 'PATCH',
        headers: {
          Authorization: `token ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          state: action === 'close' ? 'closed' : 'open',
        }),
      });

      if (response.ok) {
        fetchIssues();

        alert(
          `Issue ${action === 'close' ? 'closed' : 'updated'} successfully!`
        );
      } else {
        throw new Error('Unable to perform action');
      }
    } catch (error) {
      console.error(error);
      alert('An error occurred while performing the action.');
    }
  }
});

// Function to update an issue
async function updateIssue(issueNumber, updatedTitle, updatedDescription) {
  try {
    const response = await fetch(`${apiUrl}/${issueNumber}`, {
      method: 'PATCH',
      headers: {
        Authorization: `token ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: updatedTitle,
        body: updatedDescription,
      }),
    });

    if (response.ok) {
      fetchIssues();
      alert('Issue updated successfully!');
    } else {
      throw new Error('Unable to update issue');
    }
  } catch (error) {
    console.error(error);
    alert('An error occurred while updating the issue.');
  }
}

// Event listener to submit the update form in the modal
document.getElementById('update-form').addEventListener('submit', async (e) => {
  e.preventDefault();

  const updatedTitle = document.getElementById('update-title').value;
  const updatedDescription =
    document.getElementById('update-description').value;

  const issueNumber = document
    .getElementById('updateIssueModal')
    .getAttribute('data-issue-number');

  try {
    await updateIssue(issueNumber, updatedTitle, updatedDescription);
    closeUpdateModal();

    fetchIssues();
  } catch (error) {
    console.error(error);
    alert('An error occurred while submitting the update form.');
  }
});
